import { Robot, Ball } from '@namec/types'
import MathV from 'victor'

interface Position {
  x: number,
  y: number,
}

class NearestBot {
  static getDistance(xyA: Position, xyB: Position): number {
    const vecA = new MathV(xyA.x, xyA.y)
    const vecB = new MathV(xyB.x, xyB.y)
    const distance = vecB.distance(vecA).toFixed(3)
    return +distance
  }

  static getNearestAlliesSorted(alliesBots: Array<Robot>, ball: Ball): Map<number, number> {
    let nearestBotAllies = new Map<number, number>()
    alliesBots.forEach((ally) => {
      const distance = this.getDistance(ally.position, ball.position)
      nearestBotAllies.set(ally.id, distance)
    })
    nearestBotAllies = new Map([...nearestBotAllies.entries()].sort((a, b) => a[1] - b[1]))
    return nearestBotAllies
  }

  static getNearestOpponentsSorted(opponentsBots: Array<Robot>, ball: Ball): Map<number, number> {
    let nearestBotOpponents = new Map<number, number>()
    opponentsBots.forEach((opponent) => {
      const distance = this.getDistance(opponent.position, ball.position)
      nearestBotOpponents.set(opponent.id, distance)
    })
    nearestBotOpponents = new Map([...nearestBotOpponents.entries()].sort((a, b) => a[1] - b[1]))
    return nearestBotOpponents
  }

  static getNearbyOpponent(robot: Robot, opponents: Array<Robot>): Array<number> {
    let nearbyOpponents = new Map<number, number>()
    opponents.forEach((opponent) => {
      const distance = this.getDistance(robot.position, opponent.position)
      nearbyOpponents.set(opponent.id, distance)
    })
    nearbyOpponents = new Map([...nearbyOpponents.entries()].sort((a, b) => a[1] - b[1]))
    return Array.from(nearbyOpponents.entries().next().value)
  }

  static getNearbyAlly(robot: Robot, allies: Array<Robot>): Array<number> {
    let nearbyAllies = new Map<number, number>()
    allies.forEach((ally) => {
      const distance = this.getDistance(robot.position, ally.position)
      nearbyAllies.set(ally.id, distance)
    })
    nearbyAllies = new Map([...nearbyAllies.entries()].sort((a, b) => a[1] - b[1]))
    return Array.from(nearbyAllies.entries().next().value)
  }

  static getNearestAlly(alliesBots: Array<Robot>, ball: Ball): Array<number> {
    return Array.from(this.getNearestAlliesSorted(alliesBots, ball).entries().next().value)
  }

  static getNearestOpponent(opponentsBots: Array<Robot>, ball: Ball): Array<number> {
    return Array.from(this.getNearestOpponentsSorted(opponentsBots, ball).entries().next().value)
  }
  // TODO getMostDangerousOpponnent
}
export default NearestBot
